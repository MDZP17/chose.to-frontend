import React from "react";
import Button from "@material-ui/core/Button";

export default function Bouton({children}) {
  return (
    <Button>
      {children}
    </Button>
  );
}
